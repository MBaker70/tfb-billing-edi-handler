package com.tmobile.lambda;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class IncomingPurchaseOrderTest {
    
    @Test
    public void testProduceHumanReadableCsv() throws IOException {
        File testDir = new File("target/test/IncomingPurchaseOrder");
        testDir.mkdirs();

        IncomingPurchaseOrder purchaseOrder = new IncomingPurchaseOrder();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("edi/EDI850_Sample_txt.edi");
        OutputStream outputStream = new FileOutputStream(new File("target/test/IncomingPurchaseOrder/nominalHumanReadable.csv"));
        purchaseOrder.produceHumanReadableCsv(inputStream, outputStream);
        outputStream.close();

        InputStream compareToFile = getClass().getClassLoader().getResourceAsStream("csv/human-readable-purchase-order.csv");
        InputStream resultFile = new FileInputStream(new File("target/test/IncomingPurchaseOrder/nominalHumanReadable.csv"));
        assertTrue("Human Readable CSV does not match.", IOUtils.contentEquals(compareToFile, resultFile));
    }
}

package com.tmobile;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.tmobile.model.RequestMessage;
import com.tmobile.service.EmailService;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void orderReceiptEmailTest(){
        EmailService es=new EmailService();
        es.sendOrderReceipt("13421344");
    }

    @Test
    public void orderFailureEmailTest(){
        EmailService es=new EmailService();
        es.sendOrderFailure("13421344");
    }

    @Test
    public void invoiceSentEmailTest(){
        EmailService es=new EmailService();
        es.sendInvoiceSent("13421344", "Inseego");
    }

    @Test
    public void invoiceAcknowledgementReceivedEmailTest(){
        EmailService es=new EmailService();
        es.sendInvoiceAcknowledgementReceived("13421344", "Inseego");
    }
    /*
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    */
    /*
    @Test
    public void canRead() throws FileNotFoundException, IOException, SAXException {

        InputStream inputStream = new FileInputStream(new File("src/test/resources/edi/EDI850_Sample_txt.edi"));
        Order order = App.readEdi(inputStream);
        assertTrue(true);
        assertEquals(order.getInterchangeControlHeader().getAuthorizationInformationQualifier(),"00");
        assertEquals(order.getInterchangeControlHeader().getAuthorizationInformation(),"          ");
        assertEquals(order.getInterchangeControlHeader().getSecurityInfoQualifier(),"00");
        assertEquals(order.getInterchangeControlHeader().getSecurityInformation(),"          ");
        assertEquals(order.getInterchangeControlHeader().getInterchangeIdQualifierIsa05(),"ZZ");
        assertEquals(order.getInterchangeControlHeader().getInterchangeSenderId(),"NYCDOE         ");
        
    }
    */
    /*
    @Test
    public void canReadXML() throws FileNotFoundException, IOException, SAXException, SmooksException {
        InputStream inputStream = new FileInputStream(new File("src/test/resources/edi/EDI850_Sample_txt.txt"));
        String json = App.readEdiToXML(inputStream);
        assertTrue(true);
        System.out.println(json);
        
    }

    @Test
    public void canReadJson() throws FileNotFoundException, IOException, SAXException, SmooksException {
        InputStream inputStream = new FileInputStream(new File("src/test/resources/edi/EDI850_Sample_txt.txt"));
        String json = App.readEdiToJson(inputStream);
        assertTrue(true);
        System.out.println(json);
        
    }
    */
    /*
    @Test
    public void canReadEdiToJsonFile() throws FileNotFoundException, IOException, SAXException, SmooksException {
        InputStream inputStream = new FileInputStream(new File("src/test/resources/edi/EDI850_Sample_txt.txt"));
        App.readEdiToJsonFile(inputStream, "target/output.json", true);
        assertTrue(true);
        
    }
    */
/*
    @Test
    public void canReadJsonToJsonFile() throws FileNotFoundException, IOException, SAXException, SmooksException {
        InputStream inputStream = new FileInputStream(new File("output.json"));
        App.readJsonToJsonFile(inputStream, "target/output2.json", true);
        inputStream.close();
        assertTrue(true);
        
    }
    */

    /*
    @Test
    public void canReadJsonToEdiFile() throws FileNotFoundException, IOException, SAXException, SmooksException {
        InputStream inputStream = new FileInputStream(new File("output.json"));
        App.readJsonToEdiFile(inputStream, "target/output.edi");
        inputStream.close();
        assertTrue(true);
        
    }*/
    /*
    @Test
    public void canReadEdiToEdiFile() throws FileNotFoundException, IOException, SAXException, SmooksException {
        InputStream inputStream = new FileInputStream(new File("src/test/resources/edi/EDI850_Sample_txt.txt"));
        String xml = App.readEdiToXML(inputStream);
        inputStream.close();
        System.out.println(xml);
        App.readXmlToEdiFile(xml, "target/output.edi");
        assertTrue(true);
        
    }*/

    /*
    @Test
    public void canReadEdiToXmlFile() throws FileNotFoundException, IOException, SAXException, SmooksException {
        InputStream inputStream = new FileInputStream(new File("src/test/resources/edi/EDI850_Sample_txt.txt"));
        App.readEdiToXmlFile(inputStream, "target/output.xml", true);
        inputStream.close();
        assertTrue(true);
        
    }
    */

    /*
    @Test
    public void canReadXmlToEdiFile() throws FileNotFoundException, IOException, SAXException, SmooksException {
        InputStream inputStream = new FileInputStream(new File("output.xml"));
        App.readXmlToEdiFile(inputStream, "target/output.edi");
        inputStream.close();
        assertTrue(true);
        
    }
    */
    
    /*
    @Test
    public void dumbTest() throws FileNotFoundException, IOException, SAXException, SmooksException, JAXBException {
        App.copyOnline();
        assertTrue(true);
        
    }
    */
    
}

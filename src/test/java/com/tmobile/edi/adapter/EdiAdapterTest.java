package com.tmobile.edi.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.Diff;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Any;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.edi.model.order.Order;

import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.smooks.api.SmooksException;
import org.xml.sax.SAXException;

@Ignore
@RunWith(MockitoJUnitRunner.class)
public class EdiAdapterTest {
    
    @Mock
    private FileInputStream fileInputStreamMock;

    /*********************
     * Constructor Tests *
     *********************/

    @Test(expected = IllegalArgumentException.class)
    public void testConstructor_FormatCode_Invalid() {
        //Anything except 850 and 810 should result in an exception
        EdiAdapter cut = new EdiAdapter(997);
    }


    /***********************
     * CheckResource Tests *
     ***********************/

    /*
    * NOTE: This test serves as a record of what functionality is currently supported as it lists all transform config files
    */
    @Test
    public void testCheckResource_Resource_Exists() {
        EdiAdapter cut = new EdiAdapter(850);
        cut.checkResource("850/transform-config/bindingconfig-edi-bean.xml");
        cut.checkResource("850/transform-config/bindingconfig-json-xml.xml");
        cut.checkResource("850/transform-config/bindingconfig-xml-edi.xml");
        cut = new EdiAdapter(810);
        cut.checkResource("810/transform-config/bindingconfig-csv-xml.xml");
        cut.checkResource("810/transform-config/bindingconfig-edi-xml.xml");
        cut.checkResource("810/transform-config/bindingconfig-xml-edi.xml");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testCheckResource_Resource_DoesNotExist() {
        EdiAdapter cut = new EdiAdapter(810);
        cut.checkResource("810/transform-config/bindingconfig-I-am-not-real.xml");
    }

    /**************************
     * Nominal Transformation 
     * @throws SAXException
     * @throws IOException*
     **************************/
    @Test
    public void testRoundTripTransforms_850_Nominal() throws IOException, SAXException {
        //Prep
        File testDir = new File("target/test/850Nominal");
        testDir.mkdirs();
        EdiAdapter cut = new EdiAdapter(850);

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // Edi -> Bean
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("edi/EDI850_Sample_txt.edi");
        Order order = cut.ediToBean(inputStream);

        // Bean -> Edi
        OutputStream outputStream = new FileOutputStream(new File("target/test/850Nominal/beanToEdi.edi"));
        cut.beanToEdi(order, outputStream);
        outputStream.close();

        // Edi -> Bean -> Edi Check
        InputStream compareToFile = getClass().getClassLoader().getResourceAsStream("edi/EDI850_Sample_txt.edi");
        InputStream resultFile = new FileInputStream(new File("target/test/850Nominal/beanToEdi.edi"));
        assertTrue("RoundTrip Edi-> Bean -> Edi failed.  Produced Edi did not match", IOUtils.contentEquals(compareToFile, resultFile));

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        // Json -> Bean
        inputStream = getClass().getClassLoader().getResourceAsStream("json/850-Order.json");
        order = cut.jsonToBean(inputStream);

        // Bean -> Json
        outputStream = new FileOutputStream(new File("target/test/850Nominal/beanToJson.json"));
        cut.beanToJson(order, outputStream);
        outputStream.close();

        // Json -> Bean -> Json Check
        compareToFile = getClass().getClassLoader().getResourceAsStream("json/850-Order.json");
        byte [] compareToByteArray = new byte [compareToFile.available()];
        compareToFile.read(compareToByteArray);
        resultFile = new FileInputStream(new File("target/test/850Nominal/beanToJson.json"));
        byte [] resultByteArray = new byte [resultFile.available()];
        resultFile.read(resultByteArray);
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(compareToByteArray), mapper.readTree(resultByteArray));

        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Json -> Xml
        inputStream = getClass().getClassLoader().getResourceAsStream("json/850-Order.json");
        outputStream = new FileOutputStream(new File("target/test/850Nominal/jsonToXml.xml"));
        cut.jsonToXml(inputStream, outputStream);
        outputStream.close();

        // Xml -> Json
        inputStream = new FileInputStream(new File("target/test/850Nominal/jsonToXml.xml"));
        outputStream = new FileOutputStream(new File("target/test/850Nominal/xmlToJson.json"));
        cut.xmlToJson(inputStream, outputStream);
        outputStream.close();

        // Json -> Xml -> Json Check
        compareToFile = getClass().getClassLoader().getResourceAsStream("json/850-Order.json");
        compareToByteArray = new byte [compareToFile.available()];
        compareToFile.read(compareToByteArray);
        resultFile = new FileInputStream(new File("target/test/850Nominal/xmlToJson.json"));
        resultByteArray = new byte [resultFile.available()];
        resultFile.read(resultByteArray);
        assertEquals(mapper.readTree(compareToByteArray), mapper.readTree(resultByteArray));

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Edi -> Xml
        inputStream = getClass().getClassLoader().getResourceAsStream("edi/EDI850_Sample_txt.edi");
        outputStream = new FileOutputStream(new File("target/test/850Nominal/ediToXml.xml"));
        cut.ediToXml(inputStream, outputStream);
        outputStream.close();

        // Xml -> Edi
        inputStream = new FileInputStream(new File("target/test/850Nominal/ediToXml.xml"));
        outputStream = new FileOutputStream(new File("target/test/850Nominal/xmlToEdi.edi"));
        cut.xmlToEdi(inputStream, outputStream);
        outputStream.close();

        // Edi -> Xml -> Edi Check
        compareToFile = getClass().getClassLoader().getResourceAsStream("edi/EDI850_Sample_txt.edi");
        resultFile = new FileInputStream(new File("target/test/850Nominal/xmlToEdi.edi"));
        assertTrue("RoundTrip Edi-> Xml -> Edi failed.  Produced Edi did not match", IOUtils.contentEquals(compareToFile, resultFile));

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Edi -> Json
        inputStream = getClass().getClassLoader().getResourceAsStream("edi/EDI850_Sample_txt.edi");
        outputStream = new FileOutputStream(new File("target/test/850Nominal/ediToJson.json"));
        cut.ediToJson(inputStream, outputStream);
        outputStream.close();

        // Json -> Edi
        inputStream = new FileInputStream(new File("target/test/850Nominal/ediToJson.json"));
        outputStream = new FileOutputStream(new File("target/test/850Nominal/jsonToEdi.edi"));
        cut.jsonToEdi(inputStream, outputStream);
        outputStream.close();

        // Edi -> Xml -> Edi Check
        compareToFile = getClass().getClassLoader().getResourceAsStream("edi/EDI850_Sample_txt.edi");
        resultFile = new FileInputStream(new File("target/test/850Nominal/jsonToEdi.edi"));
        assertTrue("RoundTrip Edi-> Json -> Edi failed.  Produced Edi did not match", IOUtils.contentEquals(compareToFile, resultFile));

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Xml -> Bean
        inputStream = getClass().getClassLoader().getResourceAsStream("xml/850-Order.xml");
        order = cut.xmlToBean(inputStream);

        // Bean -> Xml
        outputStream = new FileOutputStream(new File("target/test/850Nominal/beanToXml.xml"));
        cut.beanToXml(order, outputStream);
        outputStream.close();

        // Xml -> Bean -> Xml Check
        compareToFile = getClass().getClassLoader().getResourceAsStream("xml/850-Order.xml");
        resultFile = new FileInputStream(new File("target/test/850Nominal/beanToXml.xml"));
        Diff xmlDiff = DiffBuilder.compare(Input.fromStream(compareToFile)).withTest(Input.fromStream(resultFile)).build();
        assertFalse(xmlDiff.toString(), xmlDiff.hasDifferences());
    }


    /*******************
     * EdiToBean Tests *
     *******************/

    @Test
    public void testEdiToBean_850InputStream_Nominal() throws FileNotFoundException, IOException, SAXException, SmooksException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("edi/EDI850_Sample_txt.edi");
        EdiAdapter cut = new EdiAdapter(850);
        Order order = cut.ediToBean(inputStream);
        assertTrue(true);
        assertEquals(order.getInterchangeControlHeader().getInterchangeControlNumber(),"000007277");
    }
    
    @Test(expected = UnsupportedOperationException.class)
    public void testEdiToBean_810InputStream_IsValidEdi() throws FileNotFoundException, IOException, SAXException, SmooksException {
        // TODO: Support 810 Bean
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("edi/EDI810_Sample_txt.edi");
        EdiAdapter cut = new EdiAdapter(810);
        Order order = cut.ediToBean(inputStream);
        // assertTrue(true);
        assertEquals(order.getInterchangeControlHeader().getInterchangeControlNumber(),"000007277");
    }

    @Test(expected = SmooksException.class)
    public void testEdiToBean_850InputStream_IsIncorrectEdi() throws IOException, SAXException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("edi/EDI810_Sample_txt.edi");
        EdiAdapter cut = new EdiAdapter(850);
        Order order = cut.ediToBean(inputStream);
    }

    @Test(expected = SmooksException.class)
    public void testEdiToBean_InputStream_IsNull() throws IOException, SAXException {
        EdiAdapter cut = new EdiAdapter(850);
        Order order = cut.ediToBean(null);
    }

    @Test(expected = SmooksException.class)
    public void testEdiToBean_InputStream_IsNotEdi() throws IOException, SAXException {
        EdiAdapter cut = new EdiAdapter(850);
        Order order = cut.ediToBean(new ByteArrayInputStream("This is just plain junk".getBytes()));
    }

    @Test(expected = SmooksException.class)
    public void testEdiToBean_InputStream_IOFailure() throws IOException, SAXException {
        EdiAdapter cut = new EdiAdapter(850);
        Mockito.when(fileInputStreamMock.read()).thenThrow(new IOException("Simulated IO Failure"));
        Mockito.when(fileInputStreamMock.read((byte[]) Mockito.any())).thenThrow(new IOException("Simulated IO Failure"));
        Mockito.when(fileInputStreamMock.read((byte[]) Mockito.any(), Mockito.anyInt(), Mockito.anyInt())).thenThrow(new IOException("Simulated IO Failure"));
        Order order = cut.ediToBean(fileInputStreamMock);
    }
    
    
    @Test
    public void testEdiToJson_850_Nominal() throws FileNotFoundException, IOException, SAXException, SmooksException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("edi/EDI850_Sample_txt.edi");
        EdiAdapter cut = new EdiAdapter(850);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        cut.ediToJson(inputStream, outputStream, true);

        InputStream compareToFile = getClass().getClassLoader().getResourceAsStream("json/850-Order.json");
        byte [] compareTo = new byte [compareToFile.available()];
        compareToFile.read(compareTo);
        byte [] result = outputStream.toByteArray();
        

        //For Demo Purposes
        FileWriter writer = new FileWriter(new File("target/edi-to-json-test.json"));
        writer.write(new String(result));
        writer.close();

        //Asserts
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(compareTo), mapper.readTree(result));
    }
    

    @Test
    public void canReadJsonToEdi() throws FileNotFoundException, IOException, SAXException, SmooksException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("json/850-Order.json");
        EdiAdapter cut = new EdiAdapter(850);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        cut.jsonToEdi(inputStream, outputStream);

        InputStream compareToFile = getClass().getClassLoader().getResourceAsStream("edi/EDI850_Sample_txt.edi");
        byte [] compareTo = new byte [compareToFile.available()];
        compareToFile.read(compareTo);
        byte [] result = outputStream.toByteArray();

        //For Demo Purposes
        FileWriter writer = new FileWriter(new File("target/json-to-edi-test.edi"));
        writer.write(new String(result));
        writer.close();

        //Asserts
        assertEquals(new String(result), new String(compareTo));
    }

    
    @Test
    public void canReadCsvToXml() throws FileNotFoundException, IOException, SAXException, SmooksException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("csv/daily-usage-example.csv");
        EdiAdapter cut = new EdiAdapter(810);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        cut.csvToXml(inputStream, outputStream);

        //InputStream compareToFile = getClass().getClassLoader().getResourceAsStream("edi/EDI850_Sample_txt.edi");
        //byte [] compareTo = new byte [compareToFile.available()];
        //compareToFile.read(compareTo);
        byte [] result = outputStream.toByteArray();

        //For Demo Purposes
        FileWriter writer = new FileWriter(new File("target/csv-to-xml.xml"));
        writer.write(new String(result));
        writer.close();

        //Asserts
        //assertEquals(new String(result), new String(compareTo));
    }

    @Test
    public void canReadEdiToXml() throws FileNotFoundException, IOException, SAXException, SmooksException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("edi/EDI810_Sample_txt.edi");
        EdiAdapter cut = new EdiAdapter(810);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        cut.ediToXml(inputStream, outputStream);

        //InputStream compareToFile = getClass().getClassLoader().getResourceAsStream("edi/EDI850_Sample_txt.edi");
        //byte [] compareTo = new byte [compareToFile.available()];
        //compareToFile.read(compareTo);
        byte [] result = outputStream.toByteArray();

        //For Demo Purposes
        FileWriter writer = new FileWriter(new File("target/edi-to-xml.xml"));
        writer.write(new String(result));
        writer.close();

        //Asserts
        //assertEquals(new String(result), new String(compareTo));
    }
}

package com.tmobile.service;

import com.tmobile.constants.ServiceConstants;
import com.tmobile.model.RequestMessage;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class KafkaServiceTest{
    private KafkaService ks;
    private KafkaProducer kp;
    @Before
    public void init(){
        kp = mock(KafkaProducer.class);
        ks=new KafkaService(kp);
    }

    @Test
    public void pushToQueueTest(){
        RequestMessage rm=new RequestMessage();
        ks.pushToQueue(rm);
        ArgumentCaptor<ProducerRecord> argument = ArgumentCaptor.forClass(ProducerRecord.class);
        verify(kp).send(argument.capture());
        assertEquals(ServiceConstants.KAFKA_EMAIL_TOPIC, argument.getValue().topic());
        assertNotNull(argument.getValue().value());
    }
}
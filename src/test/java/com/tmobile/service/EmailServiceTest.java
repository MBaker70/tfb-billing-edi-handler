package com.tmobile.service;

import com.tmobile.constants.ServiceConstants;
import com.tmobile.model.RequestMessage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class EmailServiceTest{
    private EmailService es;
    private KafkaService ks;
    @Before
    public void init(){
        ks = mock(KafkaService.class);
        es=new EmailService(ks);
    }

    @Test
    public void sendOrderReceiptTest(){
        String orderNumber="13421344";
        es.sendOrderReceipt(orderNumber);
        ArgumentCaptor<RequestMessage> argument = ArgumentCaptor.forClass(RequestMessage.class);
        verify(ks).pushToQueue(argument.capture());
        assertEquals(ServiceConstants.TRANS_TYPE_EDI_ORDER_RECEIVED, argument.getValue().getTransType());
        assertEquals(orderNumber, argument.getValue().getEdiOrderNumber());
    }
    @Test
    public void sendOrderFailureTest(){
        String orderNumber="13421344";
        es.sendOrderFailure(orderNumber);
        ArgumentCaptor<RequestMessage> argument = ArgumentCaptor.forClass(RequestMessage.class);
        verify(ks).pushToQueue(argument.capture());
        assertEquals(ServiceConstants.TRANS_TYPE_EDI_ORDER_FAILURE, argument.getValue().getTransType());
        assertEquals(orderNumber, argument.getValue().getEdiOrderNumber());
    }

    @Test
    public void sendInvoiceSentTest(){
        String invoiceNumber="13421344";
        String customerName="Inseego";
        es.sendInvoiceSent(invoiceNumber, customerName);
        ArgumentCaptor<RequestMessage> argument = ArgumentCaptor.forClass(RequestMessage.class);
        verify(ks).pushToQueue(argument.capture());
        assertEquals(ServiceConstants.TRANS_TYPE_EDI_INVOICE_SENT, argument.getValue().getTransType());
        assertEquals(invoiceNumber, argument.getValue().getEdiInvoiceNumber());
        assertEquals(customerName, argument.getValue().getEdiCustomerName());
    }

    @Test
    public void sendInvoiceAcknowledgementReceivedTest(){
       String invoiceNumber="13421344";
        String customerName="Inseego";
        es.sendInvoiceAcknowledgementReceived(invoiceNumber, customerName);
        ArgumentCaptor<RequestMessage> argument = ArgumentCaptor.forClass(RequestMessage.class);
        verify(ks).pushToQueue(argument.capture());
        assertEquals(ServiceConstants.TRANS_TYPE_EDI_INVOICE_ACKNOWLEDGEMENT_RECEIVED, argument.getValue().getTransType());
        assertEquals(invoiceNumber, argument.getValue().getEdiInvoiceNumber());
        assertEquals(customerName, argument.getValue().getEdiCustomerName());
    }
}
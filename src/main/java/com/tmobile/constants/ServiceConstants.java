package com.tmobile.constants;

public class ServiceConstants {
    public static final String KAFKA_BROKER_LIST = "dev2.confluent-kafka1.tfb.ccp.t-mobile.com:9092";
    //public static final String KAFKA_BROKER_LIST = "qa2.confluent-kafka1.tfb.ccp.t-mobile.com:9092,qa2.confluent-kafka2.tfb.ccp.t-mobile.com:9092,qa2.confluent-kafka3.tfb.ccp.t-mobile.com:9092"; //qa2 environment
    //public static final String KAFKA_BROKER_LIST = "prd.confluent-kafka01.tfb.ccp.t-mobile.com:9092,prd.confluent-kafka02.tfb.ccp.t-mobile.com:9092,prd.confluent-kafka03.tfb.ccp.t-mobile.com:9092,prd.confluent-kafka04.tfb.ccp.t-mobile.com:9092,prd.confluent-kafka05.tfb.ccp.t-mobile.com:9092,prd.confluent-kafka06.tfb.ccp.t-mobile.com:9092"; //prod environment
    public static final String KAFKA_EMAIL_TOPIC = "emailNotification";
    public static final String ORDER_TO_ADDRESS = "zhi.ma21@t-mobile.com"; //to address is pending
    public static final String ORDER_UI_URL = "https://www.t-mobile.com/";
    public static final String TRANS_TYPE_EDI_ORDER_RECEIVED = "EDI_ORDER_RECEIVED";
    public static final String TRANS_TYPE_EDI_ORDER_FAILURE = "EDI_ORDER_FAILURE";
    public static final String TRANS_TYPE_EDI_INVOICE_SENT = "EDI_INVOICE_SENT";
    public static final String TRANS_TYPE_EDI_INVOICE_ACKNOWLEDGEMENT_RECEIVED = "EDI_INVOICE_ACKNOWLEDGEMENT_RECEIVED";
    public static final String TXN_STATUS_SUCCESS = "S";
    public static final String TXN_STATUS_FAILURE = "F";


}

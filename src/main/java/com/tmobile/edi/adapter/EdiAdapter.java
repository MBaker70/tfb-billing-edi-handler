package com.tmobile.edi.adapter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.xml.transform.stream.StreamSource;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.tmobile.edi.model.order.Order;

import org.smooks.Smooks;
import org.smooks.api.ExecutionContext;
import org.smooks.api.SmooksException;
import org.smooks.io.payload.StringResult;
import org.xml.sax.SAXException;

public class EdiAdapter {
    enum EdiFormat {
        INVOICE_810("810", "Invoice"),
        DUD_816("816", "IDK"),
        ORDER_850("850", "PurchaseOrder"),
        DUD_855("855", "PurchaseOrderResponse"),
        DUD_997("997", "Acknowledgement");

        private final String formatCode;
        private final String name;

        private EdiFormat(String formatCode, String name) {
            this.formatCode = formatCode;
            this.name = name;
        }

        public String toString() {
            return name;
        }

        public String toCode() {
            return formatCode;
        }
    }

    private EdiFormat format;


    /**
    * Constructor
    * Currently only supports 850 and 810 formatCodes
    * TODO: support 997 and 855 and possibly 816 and 811
    *
    * @param formatCode
    */    
    public EdiAdapter(int formatCode) {
        switch (formatCode) {
            case 850:
                format = EdiFormat.ORDER_850;
                break;
            case 810:
                format = EdiFormat.INVOICE_810;
                break;
            default:
                throw new IllegalArgumentException(
                        "Unsupported Format: [" + formatCode + "].  Currently supported: [850, 810]");
        }
    }

    /**
    * Verify that the resource file exists in the jar
    * NOTE: This is not indended for external usage, but is exposed for the purpose of unit testing
    *
    * @param resourceName path to resource inside the jar
    */
    public void checkResource(String resourceName) {
        URL checkResource = getClass().getClassLoader().getResource(resourceName);
        if(checkResource == null) {
            throw new UnsupportedOperationException("Resource [" + resourceName + "] did not exist. " +
                    "This means that the configuration for this operation has not yet been defined");
        } else {
            System.out.println("Loading Resource from: " + checkResource.toString());
        }
    }

    /**
    * Converts edi into bean.
    * NOTE: This is a composite of the jsonToXml and xmlToEdi methods
    *
    * @param inputStream stream containing incoming edi
    */
    public Order ediToBean(InputStream inputStream) throws IOException, SAXException {

        String resourceName = format.toCode() + "/transform-config/bindingconfig-edi-bean.xml";
        
        checkResource(resourceName);
        
        Smooks smooks = new Smooks(getClass().getClassLoader()
                .getResourceAsStream(resourceName));
        ExecutionContext executionContext = smooks.createExecutionContext();
        StringResult result = new StringResult();
        smooks.filterSource(executionContext, new StreamSource(inputStream), result);

        Object obj = executionContext.getBeanContext().getBean("Order");

        Order order = (Order) obj;
        return order;
    }

    /**
    * Converts bean to EDI
    * TODO: not implemented
    *
    * @param inputStream stream containing incoming json
    * @param outputStream stream to recieve outgoing edi
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonGenerationException
     * @throws SAXException
    */
    public void beanToEdi(Order order, OutputStream outputStream)
                throws JsonGenerationException, JsonMappingException, IOException, SAXException {
        ByteArrayOutputStream jsonOutputStream = new ByteArrayOutputStream();
        beanToJson(order, jsonOutputStream);
        
        ByteArrayInputStream jsonInputStream = new ByteArrayInputStream(jsonOutputStream.toByteArray());
        jsonToEdi(jsonInputStream, outputStream);
    }

    /**
    * Converts bean to json
    * Note: defaults to not pretty print
    *
    * @param order incoming bean
    * @param outputStream stream to recieve outgoing json
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonGenerationException
    */
    public void beanToJson(Order order, OutputStream outputStream)
                throws JsonGenerationException, JsonMappingException, IOException {

        beanToJson(order, outputStream, false);
    }

    /**
    * Converts bean to EDI
    * TODO: not implemented
    *
    * @param order incoming bean
    * @param outputStream stream to recieve outgoing json
    * @param prettyPrint true indicates the json should be pretty printed
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonGenerationException
    */
    public void beanToJson(Order order, OutputStream outputStream, boolean prettyPrint)
                throws JsonGenerationException, JsonMappingException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.KEBAB_CASE);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        if (prettyPrint) {
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
        }

        mapper.writeValue(outputStream, order);
    }

    /**
    * Converts edi into json.
    * json will not be pretty printed.  For pretty printing use ediToJson(inputStream, outputStream, true)
    *
    * @param inputStream stream containing incoming json
    * @param outputStream stream to recieve outgoing edi
    */
    public void ediToJson(InputStream inputStream, OutputStream outputStream)
            throws JsonGenerationException, JsonMappingException, IOException, SAXException {

        ediToJson(inputStream, outputStream, false);
    }

    /**
    * Converts edi into json.
    * This uses the ediToBean method and then the beanToJson method
    *
    * @param inputStream stream containing incoming edi
    * @param outputStream stream to recieve outgoing json
    * @param prettyPrint a value of true causes the output json to be pretty printed.
    */
    public void ediToJson(InputStream inputStream, OutputStream outputStream, boolean prettyPrint)
            throws JsonGenerationException, JsonMappingException, IOException, SAXException, SmooksException {

        Order order = ediToBean(inputStream);

        beanToJson(order, outputStream, prettyPrint);
    }

    /**
    * Converts json into xml.
    *
    * @param inputStream stream containing incoming json
    * @param outputStream stream to recieve outgoing xml
    */
    public void jsonToXml(InputStream inputStream, OutputStream outputStream) throws IOException, SAXException {
        String resourceName = format.toCode() + "/transform-config/bindingconfig-json-xml.xml";
        
        checkResource(resourceName);

        Smooks smooks = new Smooks(getClass().getClassLoader()
                .getResourceAsStream(resourceName));
        ExecutionContext executionContext = smooks.createExecutionContext();
        
        StringResult xmlResult = new StringResult();
        smooks.filterSource(executionContext, new StreamSource(inputStream), xmlResult);

        String xml = xmlResult.toString();

        outputStream.write(xml.getBytes());
    }

    /**
    * Converts json into xml.
    *
    * @param inputStream stream containing incoming json
    * @param outputStream stream to recieve outgoing xml
    */
    public void xmlToJson(InputStream inputStream, OutputStream outputStream) throws IOException, SAXException {
        Order order = xmlToBean(inputStream);

        beanToJson(order, outputStream);
    }

    public Order xmlToBean(InputStream inputStream) throws IOException, SAXException {
        String resourceName = format.toCode() + "/transform-config/bindingconfig-xml-bean.xml";
        
        checkResource(resourceName);
        
        Smooks smooks = new Smooks(getClass().getClassLoader()
                .getResourceAsStream(resourceName));
        ExecutionContext executionContext = smooks.createExecutionContext();
        StringResult result = new StringResult();
        smooks.filterSource(executionContext, new StreamSource(inputStream), result);

        Object obj = executionContext.getBeanContext().getBean("Order");

        Order order = (Order) obj;
        return order;
    }

    /**
    * Converts json into edi.
    * NOTE: This is a composite of the jsonToXml and xmlToEdi methods
    *
    * @param inputStream stream containing incoming json
    * @param outputStream stream to recieve outgoing edi
    */
    public void jsonToEdi(InputStream inputStream, OutputStream outputStream) throws IOException, SAXException {

        ByteArrayOutputStream xmlOutputStream = new ByteArrayOutputStream();

        jsonToXml(inputStream, xmlOutputStream);

        ByteArrayInputStream xmlInputStream = new ByteArrayInputStream(xmlOutputStream.toByteArray());

        xmlToEdi(xmlInputStream, outputStream);
    }

    /**
    * Converts xml into edi
    *
    * @param inputStream stream containing incoming xml
    * @param outputStream stream to recieve outgoing edi
    */
    public void xmlToEdi(InputStream inputStream, OutputStream outputStream) throws IOException, SAXException {
        String resourceName = format.toCode() + "/transform-config/bindingconfig-xml-edi.xml";
        
        checkResource(resourceName);

        Smooks smooks = new Smooks(getClass().getClassLoader().getResourceAsStream(resourceName));
        ExecutionContext executionContext = smooks.createExecutionContext();
        
        StringResult ediResult = new StringResult();
        smooks.filterSource(executionContext, new StreamSource(inputStream), ediResult);

        String edi = ediResult.toString();

        outputStream.write(edi.getBytes());
    }

    /**
    * Converts edi into xml
    *
    * @param inputStream stream containing incoming edi
    * @param outputStream stream to recieve outgoing xml
    */
    public void ediToXml(InputStream inputStream, OutputStream outputStream) throws IOException, SAXException {
        String resourceName = format.toCode() + "/transform-config/bindingconfig-edi-xml.xml";
        
        checkResource(resourceName);

        Smooks smooks = new Smooks(
                getClass().getClassLoader().getResourceAsStream(resourceName));
        ExecutionContext executionContext = smooks.createExecutionContext();

        StringResult xmlResult = new StringResult();
        smooks.filterSource(executionContext, new StreamSource(inputStream), xmlResult);
        String result = xmlResult.getResult();

        outputStream.write(result.getBytes());
    }

    /**
    * Converts json into bean
    * NOTE: This is a work in progress and only currently support order 850
    *
    * @param inputStream stream containing incoming json
    */
    public Order jsonToBean(InputStream inputStream) throws IOException, SAXException {
        String resourceName = format.toCode() + "/transform-config/bindingconfig-json-bean.xml";
        
        checkResource(resourceName);

        Smooks smooks = new Smooks(getClass().getClassLoader()
                .getResourceAsStream(resourceName));
        ExecutionContext executionContext = smooks.createExecutionContext();

        StringResult xmlResult = new StringResult();
        smooks.filterSource(executionContext, new StreamSource(inputStream), xmlResult);

        Object obj = executionContext.getBeanContext().getBean("Order");
        return (Order) obj;
    }

    public void beanToXml(Order order, OutputStream outputStream)
                throws JsonGenerationException, JsonMappingException, IOException, SAXException {
        ByteArrayOutputStream jsonOutputStream = new ByteArrayOutputStream();
        beanToJson(order, jsonOutputStream);
        
        ByteArrayInputStream jsonInputStream = new ByteArrayInputStream(jsonOutputStream.toByteArray());
        jsonToXml(jsonInputStream, outputStream);
    }

    /**
    * Converts csv into xml
    * NOTE: This is a work in progress and only currently support invoice 810
    *
    * @param inputStream stream containing incoming csv
    * @param outputStream stream to recieve outgoing xml
    */
    public void csvToXml(InputStream inputStream, OutputStream outputStream) throws IOException, SAXException {
        String resourceName = format.toCode() + "/transform-config/bindingconfig-csv-xml.xml";
        
        checkResource(resourceName);

        Smooks smooks = new Smooks(
                getClass().getClassLoader().getResourceAsStream(resourceName));
        ExecutionContext executionContext = smooks.createExecutionContext();

        StringResult xmlResult = new StringResult();
        smooks.filterSource(executionContext, new StreamSource(inputStream), xmlResult);
        String result = xmlResult.getResult();
        
        outputStream.write(result.getBytes());
    }

    public void ediToCsv(InputStream inputStream, OutputStream outputStream) throws IOException, SAXException {
        //TODO: Implement for 850
    }
}
/**
 * This class was generated by Smooks EJC (http://www.smooks.org).
 */
package com.tmobile.edi.model.order;

import java.io.Serializable;    
import org.milyn.smooks.edi.EDIWritable;    
import java.util.List;    
import java.io.Writer;    
import org.milyn.edisax.model.internal.Delimiters;    
import java.io.IOException;    

public class N1 implements Serializable, EDIWritable {

    private static final long serialVersionUID = 1L;

    private Name name;
    private List<AdditionalNameInformation> additionalNameInformation;
    private List<AddressInformation> addressInformation;
    private GeographicLocation geographicLocation;
    private List<AdministrativeCommunicationsContact> administrativeCommunicationsContact;

    public void write(Writer writer, Delimiters delimiters) throws IOException {
        
        Writer nodeWriter = writer;

        if(name != null) {
            nodeWriter.write("N1");
            nodeWriter.write(delimiters.getField());
            name.write(nodeWriter, delimiters);
        }
        if(additionalNameInformation != null && !additionalNameInformation.isEmpty()) {
            for(AdditionalNameInformation additionalNameInformationInst : additionalNameInformation) {
                nodeWriter.write("N2");
                nodeWriter.write(delimiters.getField());
                additionalNameInformationInst.write(nodeWriter, delimiters);
            }
        }
        if(addressInformation != null && !addressInformation.isEmpty()) {
            for(AddressInformation addressInformationInst : addressInformation) {
                nodeWriter.write("N3");
                nodeWriter.write(delimiters.getField());
                addressInformationInst.write(nodeWriter, delimiters);
            }
        }
        if(geographicLocation != null) {
            nodeWriter.write("N4");
            nodeWriter.write(delimiters.getField());
            geographicLocation.write(nodeWriter, delimiters);
        }
        if(administrativeCommunicationsContact != null && !administrativeCommunicationsContact.isEmpty()) {
            for(AdministrativeCommunicationsContact administrativeCommunicationsContactInst : administrativeCommunicationsContact) {
                nodeWriter.write("PER");
                nodeWriter.write(delimiters.getField());
                administrativeCommunicationsContactInst.write(nodeWriter, delimiters);
            }
        }
    }

    public Name getName() {
        return name;
    }

    public N1 setName(Name name) {
        this.name = name;  return this;
    }

    public List<AdditionalNameInformation> getAdditionalNameInformation() {
        return additionalNameInformation;
    }

    public N1 setAdditionalNameInformation(List<AdditionalNameInformation> additionalNameInformation) {
        this.additionalNameInformation = additionalNameInformation;  return this;
    }

    public List<AddressInformation> getAddressInformation() {
        return addressInformation;
    }

    public N1 setAddressInformation(List<AddressInformation> addressInformation) {
        this.addressInformation = addressInformation;  return this;
    }

    public GeographicLocation getGeographicLocation() {
        return geographicLocation;
    }

    public N1 setGeographicLocation(GeographicLocation geographicLocation) {
        this.geographicLocation = geographicLocation;  return this;
    }

    public List<AdministrativeCommunicationsContact> getAdministrativeCommunicationsContact() {
        return administrativeCommunicationsContact;
    }

    public N1 setAdministrativeCommunicationsContact(List<AdministrativeCommunicationsContact> administrativeCommunicationsContact) {
        this.administrativeCommunicationsContact = administrativeCommunicationsContact;  return this;
    }
}
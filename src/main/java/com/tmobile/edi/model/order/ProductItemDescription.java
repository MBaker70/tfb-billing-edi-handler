/**
 * This class was generated by Smooks EJC (http://www.smooks.org).
 */
package com.tmobile.edi.model.order;

import java.io.Serializable;    
import org.milyn.smooks.edi.EDIWritable;    
import java.io.Writer;    
import org.milyn.edisax.model.internal.Delimiters;    
import java.io.IOException;    
import java.io.StringWriter;    
import java.util.List;    
import java.util.ArrayList;    
import org.milyn.edisax.util.EDIUtils;    
import org.milyn.edisax.model.internal.DelimiterType;    

public class ProductItemDescription implements Serializable, EDIWritable {

    private static final long serialVersionUID = 1L;

    private String itemDescriptionType;
    private String dud1;
    private String dud2;
    private String dud3;
    private String description;

    public void write(Writer writer, Delimiters delimiters) throws IOException {
        
        Writer nodeWriter = new StringWriter();

        List<String> nodeTokens = new ArrayList<String>();

        if(itemDescriptionType != null) {
            nodeWriter.write(delimiters.escape(itemDescriptionType.toString()));
            nodeTokens.add(nodeWriter.toString());
            ((StringWriter)nodeWriter).getBuffer().setLength(0);
        }
        nodeWriter.write(delimiters.getField());
        if(dud1 != null) {
            nodeWriter.write(delimiters.escape(dud1.toString()));
            nodeTokens.add(nodeWriter.toString());
            ((StringWriter)nodeWriter).getBuffer().setLength(0);
        }
        nodeWriter.write(delimiters.getField());
        if(dud2 != null) {
            nodeWriter.write(delimiters.escape(dud2.toString()));
            nodeTokens.add(nodeWriter.toString());
            ((StringWriter)nodeWriter).getBuffer().setLength(0);
        }
        nodeWriter.write(delimiters.getField());
        if(dud3 != null) {
            nodeWriter.write(delimiters.escape(dud3.toString()));
            nodeTokens.add(nodeWriter.toString());
            ((StringWriter)nodeWriter).getBuffer().setLength(0);
        }
        nodeWriter.write(delimiters.getField());
        if(description != null) {
            nodeWriter.write(delimiters.escape(description.toString()));
            nodeTokens.add(nodeWriter.toString());
            ((StringWriter)nodeWriter).getBuffer().setLength(0);
        }
        nodeTokens.add(nodeWriter.toString());
        writer.write(EDIUtils.concatAndTruncate(nodeTokens, DelimiterType.FIELD, delimiters));
        writer.write(delimiters.getSegmentDelimiter());
        writer.flush();
    }

    public String getItemDescriptionType() {
        return itemDescriptionType;
    }

    public ProductItemDescription setItemDescriptionType(String itemDescriptionType) {
        this.itemDescriptionType = itemDescriptionType;  return this;
    }

    public String getDud1() {
        return dud1;
    }

    public ProductItemDescription setDud1(String dud1) {
        this.dud1 = dud1;  return this;
    }

    public String getDud2() {
        return dud2;
    }

    public ProductItemDescription setDud2(String dud2) {
        this.dud2 = dud2;  return this;
    }

    public String getDud3() {
        return dud3;
    }

    public ProductItemDescription setDud3(String dud3) {
        this.dud3 = dud3;  return this;
    }

    public String getDescription() {
        return description;
    }

    public ProductItemDescription setDescription(String description) {
        this.description = description;  return this;
    }
}
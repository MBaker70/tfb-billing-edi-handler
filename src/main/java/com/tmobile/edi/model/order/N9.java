/**
 * This class was generated by Smooks EJC (http://www.smooks.org).
 */
package com.tmobile.edi.model.order;

import java.io.Serializable;    
import org.milyn.smooks.edi.EDIWritable;    
import java.util.List;    
import java.io.Writer;    
import org.milyn.edisax.model.internal.Delimiters;    
import java.io.IOException;    

public class N9 implements Serializable, EDIWritable {

    private static final long serialVersionUID = 1L;

    private ReferenceIdentificationComments referenceIdentificationComments;
    private DateTimeReference dateTimeReference;
    private List<MessageText> messageText;

    public void write(Writer writer, Delimiters delimiters) throws IOException {
        
        Writer nodeWriter = writer;

        if(referenceIdentificationComments != null) {
            nodeWriter.write("N9");
            nodeWriter.write(delimiters.getField());
            referenceIdentificationComments.write(nodeWriter, delimiters);
        }
        if(dateTimeReference != null) {
            nodeWriter.write("DTM");
            nodeWriter.write(delimiters.getField());
            dateTimeReference.write(nodeWriter, delimiters);
        }
        if(messageText != null && !messageText.isEmpty()) {
            for(MessageText messageTextInst : messageText) {
                nodeWriter.write("MSG");
                nodeWriter.write(delimiters.getField());
                messageTextInst.write(nodeWriter, delimiters);
            }
        }
    }

    public ReferenceIdentificationComments getReferenceIdentificationComments() {
        return referenceIdentificationComments;
    }

    public N9 setReferenceIdentificationComments(ReferenceIdentificationComments referenceIdentificationComments) {
        this.referenceIdentificationComments = referenceIdentificationComments;  return this;
    }

    public DateTimeReference getDateTimeReference() {
        return dateTimeReference;
    }

    public N9 setDateTimeReference(DateTimeReference dateTimeReference) {
        this.dateTimeReference = dateTimeReference;  return this;
    }

    public List<MessageText> getMessageText() {
        return messageText;
    }

    public N9 setMessageText(List<MessageText> messageText) {
        this.messageText = messageText;  return this;
    }
}
/**
 * Generated by Smooks EJC (Edifact Java Compiler).
 */
package com.tmobile.edi.model.order;

import org.milyn.Smooks;
import org.milyn.payload.JavaResult;
import org.milyn.edisax.model.internal.Edimap;
import org.milyn.edisax.model.internal.Delimiters;
import org.milyn.edisax.model.EDIConfigDigester;
import org.milyn.edisax.EDIConfigurationException;
import org.xml.sax.SAXException;

import javax.xml.transform.stream.StreamSource;
import java.io.Reader;
import java.io.Writer;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.IOException;
import javax.xml.transform.Result;

public class OrderFactory {

    private Smooks smooks;
    private Delimiters delimiters;

    public static OrderFactory getInstance() throws IOException, SAXException {
        return new OrderFactory();
    }

    public void addConfigurations(InputStream resourceConfigStream) throws SAXException, IOException {
        smooks.addConfigurations(resourceConfigStream);
    }

    public Order fromEDI(InputStream ediStream, Result... additionalResults) {
        return fromEDI(new InputStreamReader(ediStream), additionalResults);
    }

    public Order fromEDI(Reader ediStream, Result... additionalResults) {
        JavaResult javaResult = new JavaResult();
        int numAdditionalRes = (additionalResults != null? additionalResults.length : 0);
        Result[] results = new Result[numAdditionalRes + 1];

        results[0] = javaResult;
        if(additionalResults != null) {
            System.arraycopy(additionalResults, 0, results, 1, numAdditionalRes);
        }

        smooks.filterSource(new StreamSource(ediStream), results);
        return (Order) javaResult.getBean(Order.class);
    }


    public void toEDI(Order instance, Writer writer) throws IOException {
        instance.write(writer, delimiters);
    }

    private OrderFactory() throws IOException, SAXException {
        smooks = new Smooks(OrderFactory.class.getResourceAsStream("bindingconfig.xml"));

        try {
            Edimap edimap = EDIConfigDigester.digestConfig(OrderFactory.class.getResourceAsStream("edimappingconfig.xml"));
            delimiters = edimap.getDelimiters();
        } catch(EDIConfigurationException e) {
            IOException ioException = new IOException("Exception reading EDI Mapping model.");
            ioException.initCause(e);
            throw ioException;
        }
    }
}
/**
 * This class was generated by Smooks EJC (http://www.smooks.org).
 */
package com.tmobile.edi.model.order;

import java.io.Serializable;    
import org.milyn.smooks.edi.EDIWritable;    
import java.io.Writer;    
import org.milyn.edisax.model.internal.Delimiters;    
import java.io.IOException;    

public class Ctt implements Serializable, EDIWritable {

    private static final long serialVersionUID = 1L;

    private TransactionTotals transactionTotals;
    private MonetaryAmount monetaryAmount;

    public void write(Writer writer, Delimiters delimiters) throws IOException {
        
        Writer nodeWriter = writer;

        if(transactionTotals != null) {
            nodeWriter.write("CTT");
            nodeWriter.write(delimiters.getField());
            transactionTotals.write(nodeWriter, delimiters);
        }
        if(monetaryAmount != null) {
            nodeWriter.write("AMT");
            nodeWriter.write(delimiters.getField());
            monetaryAmount.write(nodeWriter, delimiters);
        }
    }

    public TransactionTotals getTransactionTotals() {
        return transactionTotals;
    }

    public Ctt setTransactionTotals(TransactionTotals transactionTotals) {
        this.transactionTotals = transactionTotals;  return this;
    }

    public MonetaryAmount getMonetaryAmount() {
        return monetaryAmount;
    }

    public Ctt setMonetaryAmount(MonetaryAmount monetaryAmount) {
        this.monetaryAmount = monetaryAmount;  return this;
    }
}
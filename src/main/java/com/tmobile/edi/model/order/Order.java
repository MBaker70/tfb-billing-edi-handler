/**
 * This class was generated by Smooks EJC (http://www.smooks.org).
 */
package com.tmobile.edi.model.order;

import java.io.Serializable;    
import org.milyn.smooks.edi.EDIWritable;    
import org.milyn.smooks.edi.EDIMessage;    
import java.util.List;    
import java.io.Writer;    
import org.milyn.edisax.model.internal.Delimiters;    
import java.io.IOException;    

@EDIMessage    
public class Order implements Serializable, EDIWritable {

    private static final long serialVersionUID = 1L;

    private InterchangeControlHeader interchangeControlHeader;
    private FunctionalGroupHeader functionalGroupHeader;
    private TransactionSetHeader transactionSetHeader;
    private BeginningSegmentForPurchaseOrder beginningSegmentForPurchaseOrder;
    private List<AdministrativeCommunicationsContact> administrativeCommunicationsContact;
    private List<N9> n9;
    private List<N1> n1;
    private List<Po1> po1;
    private Ctt ctt;
    private TransactionSetTrailer transactionSetTrailer;
    private FunctionalGroupTrailer functionalGroupTrailer;
    private InterchangeControlTrailer interchangeControlTrailer;

    public void write(Writer writer, Delimiters delimiters) throws IOException {
        
        Writer nodeWriter = writer;

        if(interchangeControlHeader != null) {
            nodeWriter.write("ISA");
            nodeWriter.write(delimiters.getField());
            interchangeControlHeader.write(nodeWriter, delimiters);
        }
        if(functionalGroupHeader != null) {
            nodeWriter.write("GS");
            nodeWriter.write(delimiters.getField());
            functionalGroupHeader.write(nodeWriter, delimiters);
        }
        if(transactionSetHeader != null) {
            nodeWriter.write("ST");
            nodeWriter.write(delimiters.getField());
            transactionSetHeader.write(nodeWriter, delimiters);
        }
        if(beginningSegmentForPurchaseOrder != null) {
            nodeWriter.write("BEG");
            nodeWriter.write(delimiters.getField());
            beginningSegmentForPurchaseOrder.write(nodeWriter, delimiters);
        }
        if(administrativeCommunicationsContact != null && !administrativeCommunicationsContact.isEmpty()) {
            for(AdministrativeCommunicationsContact administrativeCommunicationsContactInst : administrativeCommunicationsContact) {
                nodeWriter.write("PER");
                nodeWriter.write(delimiters.getField());
                administrativeCommunicationsContactInst.write(nodeWriter, delimiters);
            }
        }
        if(n9 != null && !n9.isEmpty()) {
            for(N9 n9Inst : n9) {
                n9Inst.write(nodeWriter, delimiters);
            }
        }
        if(n1 != null && !n1.isEmpty()) {
            for(N1 n1Inst : n1) {
                n1Inst.write(nodeWriter, delimiters);
            }
        }
        if(po1 != null && !po1.isEmpty()) {
            for(Po1 po1Inst : po1) {
                po1Inst.write(nodeWriter, delimiters);
            }
        }
        if(ctt != null) {
            ctt.write(nodeWriter, delimiters);
        }
        if(transactionSetTrailer != null) {
            nodeWriter.write("SE");
            nodeWriter.write(delimiters.getField());
            transactionSetTrailer.write(nodeWriter, delimiters);
        }
        if(functionalGroupTrailer != null) {
            nodeWriter.write("GE");
            nodeWriter.write(delimiters.getField());
            functionalGroupTrailer.write(nodeWriter, delimiters);
        }
        if(interchangeControlTrailer != null) {
            nodeWriter.write("IEA");
            nodeWriter.write(delimiters.getField());
            interchangeControlTrailer.write(nodeWriter, delimiters);
        }
    }

    public InterchangeControlHeader getInterchangeControlHeader() {
        return interchangeControlHeader;
    }

    public Order setInterchangeControlHeader(InterchangeControlHeader interchangeControlHeader) {
        this.interchangeControlHeader = interchangeControlHeader;  return this;
    }

    public FunctionalGroupHeader getFunctionalGroupHeader() {
        return functionalGroupHeader;
    }

    public Order setFunctionalGroupHeader(FunctionalGroupHeader functionalGroupHeader) {
        this.functionalGroupHeader = functionalGroupHeader;  return this;
    }

    public TransactionSetHeader getTransactionSetHeader() {
        return transactionSetHeader;
    }

    public Order setTransactionSetHeader(TransactionSetHeader transactionSetHeader) {
        this.transactionSetHeader = transactionSetHeader;  return this;
    }

    public BeginningSegmentForPurchaseOrder getBeginningSegmentForPurchaseOrder() {
        return beginningSegmentForPurchaseOrder;
    }

    public Order setBeginningSegmentForPurchaseOrder(BeginningSegmentForPurchaseOrder beginningSegmentForPurchaseOrder) {
        this.beginningSegmentForPurchaseOrder = beginningSegmentForPurchaseOrder;  return this;
    }

    public List<AdministrativeCommunicationsContact> getAdministrativeCommunicationsContact() {
        return administrativeCommunicationsContact;
    }

    public Order setAdministrativeCommunicationsContact(List<AdministrativeCommunicationsContact> administrativeCommunicationsContact) {
        this.administrativeCommunicationsContact = administrativeCommunicationsContact;  return this;
    }

    public List<N9> getN9() {
        return n9;
    }

    public Order setN9(List<N9> n9) {
        this.n9 = n9;  return this;
    }

    public List<N1> getN1() {
        return n1;
    }

    public Order setN1(List<N1> n1) {
        this.n1 = n1;  return this;
    }

    public List<Po1> getPo1() {
        return po1;
    }

    public Order setPo1(List<Po1> po1) {
        this.po1 = po1;  return this;
    }

    public Ctt getCtt() {
        return ctt;
    }

    public Order setCtt(Ctt ctt) {
        this.ctt = ctt;  return this;
    }

    public TransactionSetTrailer getTransactionSetTrailer() {
        return transactionSetTrailer;
    }

    public Order setTransactionSetTrailer(TransactionSetTrailer transactionSetTrailer) {
        this.transactionSetTrailer = transactionSetTrailer;  return this;
    }

    public FunctionalGroupTrailer getFunctionalGroupTrailer() {
        return functionalGroupTrailer;
    }

    public Order setFunctionalGroupTrailer(FunctionalGroupTrailer functionalGroupTrailer) {
        this.functionalGroupTrailer = functionalGroupTrailer;  return this;
    }

    public InterchangeControlTrailer getInterchangeControlTrailer() {
        return interchangeControlTrailer;
    }

    public Order setInterchangeControlTrailer(InterchangeControlTrailer interchangeControlTrailer) {
        this.interchangeControlTrailer = interchangeControlTrailer;  return this;
    }
}
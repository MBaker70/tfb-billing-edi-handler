/**
 * This class was generated by Smooks EJC (http://www.smooks.org).
 */
package com.tmobile.edi.model.order;

import java.io.Serializable;    
import org.milyn.smooks.edi.EDIWritable;    
import org.milyn.javabean.decoders.IntegerDecoder;    
import java.io.Writer;    
import org.milyn.edisax.model.internal.Delimiters;    
import java.io.IOException;    
import java.io.StringWriter;    
import java.util.List;    
import java.util.ArrayList;    
import org.milyn.edisax.util.EDIUtils;    
import org.milyn.edisax.model.internal.DelimiterType;    

public class TransactionTotals implements Serializable, EDIWritable {

    private static final long serialVersionUID = 1L;

    private Integer numberOfLineItems;
    private IntegerDecoder numberOfLineItemsEncoder;

    public TransactionTotals() {
        
        numberOfLineItemsEncoder = new IntegerDecoder();
    }

    public void write(Writer writer, Delimiters delimiters) throws IOException {
        
        Writer nodeWriter = new StringWriter();

        List<String> nodeTokens = new ArrayList<String>();

        if(numberOfLineItems != null) {
            nodeWriter.write(delimiters.escape(numberOfLineItemsEncoder.encode(numberOfLineItems)));
            nodeTokens.add(nodeWriter.toString());
            ((StringWriter)nodeWriter).getBuffer().setLength(0);
        }
        nodeTokens.add(nodeWriter.toString());
        writer.write(EDIUtils.concatAndTruncate(nodeTokens, DelimiterType.FIELD, delimiters));
        writer.write(delimiters.getSegmentDelimiter());
        writer.flush();
    }

    public Integer getNumberOfLineItems() {
        return numberOfLineItems;
    }

    public TransactionTotals setNumberOfLineItems(Integer numberOfLineItems) {
        this.numberOfLineItems = numberOfLineItems;  return this;
    }
}
/**
 * This class was generated by Smooks EJC (http://www.smooks.org).
 */
package com.tmobile.edi.model.acknowledgement;

import java.io.Serializable;    
import org.milyn.smooks.edi.EDIWritable;    
import java.io.Writer;    
import org.milyn.edisax.model.internal.Delimiters;    
import java.io.IOException;    
import java.io.StringWriter;    
import java.util.List;    
import java.util.ArrayList;    
import org.milyn.edisax.util.EDIUtils;    
import org.milyn.edisax.model.internal.DelimiterType;    

public class FunctionalGroupHeader implements Serializable, EDIWritable {

    private static final long serialVersionUID = 1L;

    private String functionalIdCode;
    private String applicationSendersCode;
    private String applicationReceiversCode;
    private String dataInterchangeDate;
    private String dataInterchangeTime;
    private String dataInterchangeControlNumber;
    private String responsibleAgencyCode;
    private String versionReleaseIdCode;

    public void write(Writer writer, Delimiters delimiters) throws IOException {
        
        Writer nodeWriter = new StringWriter();

        List<String> nodeTokens = new ArrayList<String>();

        if(functionalIdCode != null) {
            nodeWriter.write(delimiters.escape(functionalIdCode.toString()));
            nodeTokens.add(nodeWriter.toString());
            ((StringWriter)nodeWriter).getBuffer().setLength(0);
        }
        nodeWriter.write(delimiters.getField());
        if(applicationSendersCode != null) {
            nodeWriter.write(delimiters.escape(applicationSendersCode.toString()));
            nodeTokens.add(nodeWriter.toString());
            ((StringWriter)nodeWriter).getBuffer().setLength(0);
        }
        nodeWriter.write(delimiters.getField());
        if(applicationReceiversCode != null) {
            nodeWriter.write(delimiters.escape(applicationReceiversCode.toString()));
            nodeTokens.add(nodeWriter.toString());
            ((StringWriter)nodeWriter).getBuffer().setLength(0);
        }
        nodeWriter.write(delimiters.getField());
        if(dataInterchangeDate != null) {
            nodeWriter.write(delimiters.escape(dataInterchangeDate.toString()));
            nodeTokens.add(nodeWriter.toString());
            ((StringWriter)nodeWriter).getBuffer().setLength(0);
        }
        nodeWriter.write(delimiters.getField());
        if(dataInterchangeTime != null) {
            nodeWriter.write(delimiters.escape(dataInterchangeTime.toString()));
            nodeTokens.add(nodeWriter.toString());
            ((StringWriter)nodeWriter).getBuffer().setLength(0);
        }
        nodeWriter.write(delimiters.getField());
        if(dataInterchangeControlNumber != null) {
            nodeWriter.write(delimiters.escape(dataInterchangeControlNumber.toString()));
            nodeTokens.add(nodeWriter.toString());
            ((StringWriter)nodeWriter).getBuffer().setLength(0);
        }
        nodeWriter.write(delimiters.getField());
        if(responsibleAgencyCode != null) {
            nodeWriter.write(delimiters.escape(responsibleAgencyCode.toString()));
            nodeTokens.add(nodeWriter.toString());
            ((StringWriter)nodeWriter).getBuffer().setLength(0);
        }
        nodeWriter.write(delimiters.getField());
        if(versionReleaseIdCode != null) {
            nodeWriter.write(delimiters.escape(versionReleaseIdCode.toString()));
            nodeTokens.add(nodeWriter.toString());
            ((StringWriter)nodeWriter).getBuffer().setLength(0);
        }
        nodeTokens.add(nodeWriter.toString());
        writer.write(EDIUtils.concatAndTruncate(nodeTokens, DelimiterType.FIELD, delimiters));
        writer.write(delimiters.getSegmentDelimiter());
        writer.flush();
    }

    public String getFunctionalIdCode() {
        return functionalIdCode;
    }

    public FunctionalGroupHeader setFunctionalIdCode(String functionalIdCode) {
        this.functionalIdCode = functionalIdCode;  return this;
    }

    public String getApplicationSendersCode() {
        return applicationSendersCode;
    }

    public FunctionalGroupHeader setApplicationSendersCode(String applicationSendersCode) {
        this.applicationSendersCode = applicationSendersCode;  return this;
    }

    public String getApplicationReceiversCode() {
        return applicationReceiversCode;
    }

    public FunctionalGroupHeader setApplicationReceiversCode(String applicationReceiversCode) {
        this.applicationReceiversCode = applicationReceiversCode;  return this;
    }

    public String getDataInterchangeDate() {
        return dataInterchangeDate;
    }

    public FunctionalGroupHeader setDataInterchangeDate(String dataInterchangeDate) {
        this.dataInterchangeDate = dataInterchangeDate;  return this;
    }

    public String getDataInterchangeTime() {
        return dataInterchangeTime;
    }

    public FunctionalGroupHeader setDataInterchangeTime(String dataInterchangeTime) {
        this.dataInterchangeTime = dataInterchangeTime;  return this;
    }

    public String getDataInterchangeControlNumber() {
        return dataInterchangeControlNumber;
    }

    public FunctionalGroupHeader setDataInterchangeControlNumber(String dataInterchangeControlNumber) {
        this.dataInterchangeControlNumber = dataInterchangeControlNumber;  return this;
    }

    public String getResponsibleAgencyCode() {
        return responsibleAgencyCode;
    }

    public FunctionalGroupHeader setResponsibleAgencyCode(String responsibleAgencyCode) {
        this.responsibleAgencyCode = responsibleAgencyCode;  return this;
    }

    public String getVersionReleaseIdCode() {
        return versionReleaseIdCode;
    }

    public FunctionalGroupHeader setVersionReleaseIdCode(String versionReleaseIdCode) {
        this.versionReleaseIdCode = versionReleaseIdCode;  return this;
    }
}
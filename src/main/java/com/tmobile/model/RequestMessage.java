package com.tmobile.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestMessage implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1488601400275063504L;

	private String transType;
	private String email;
	private String ediOrderNumber;
	private String ediOrderUIUrl;
	private String ediInvoiceNumber;
	private String ediCustomerName;
	private String txnStatus;

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEdiOrderNumber() {
		return ediOrderNumber;
	}

	public void setEdiOrderNumber(String ediOrderNumber) {
		this.ediOrderNumber = ediOrderNumber;
	}

	public String getEdiOrderUIUrl() {
		return ediOrderUIUrl;
	}

	public void setEdiOrderUIUrl(String ediOrderUIUrl) {
		this.ediOrderUIUrl = ediOrderUIUrl;
	}

	public String getEdiInvoiceNumber() {
		return ediInvoiceNumber;
	}

	public void setEdiInvoiceNumber(String ediInvoiceNumber) {
		this.ediInvoiceNumber = ediInvoiceNumber;
	}

	public String getEdiCustomerName() {
		return ediCustomerName;
	}

	public void setEdiCustomerName(String ediCustomerName) {
		this.ediCustomerName = ediCustomerName;
	}

	public String getTxnStatus() {
		return txnStatus;
	}

	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}
}

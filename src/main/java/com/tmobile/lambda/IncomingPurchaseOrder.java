package com.tmobile.lambda;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.glacier.AmazonGlacier;
import com.amazonaws.services.glacier.AmazonGlacierClientBuilder;
import com.amazonaws.services.glacier.model.UploadArchiveRequest;
import com.amazonaws.services.glacier.model.UploadArchiveResult;
import com.amazonaws.services.glacier.transfer.ArchiveTransferManager;
import com.amazonaws.services.glacier.transfer.ArchiveTransferManagerBuilder;
import com.amazonaws.services.glacier.transfer.UploadResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.models.s3.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.ibm.icu.impl.Pair;
import com.tmobile.edi.adapter.EdiAdapter;
import com.tmobile.edi.model.order.Order;
import com.tmobile.edi.model.order.Pid;
import com.tmobile.edi.model.order.Po1;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.input.TeeInputStream;
import org.apache.xerces.impl.XMLDocumentFragmentScannerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class IncomingPurchaseOrder implements RequestHandler<S3Event, String> {

    private static final Logger log = LoggerFactory.getLogger(IncomingPurchaseOrder.class);
    private static final String edi850SourceBucket = "test-doe-1";
    private static final String csvTargetBucket = "test-doe-2";
    private static final String edi997TargetBucket = "test-doe-2";
    private static final String vaultName = "glacierVault";

    private AmazonS3 s3Client;
    private EdiAdapter edi850Adapter;

    public IncomingPurchaseOrder() {
        //s3Client = AmazonS3ClientBuilder.defaultClient();
        s3Client = null;
        edi850Adapter = new EdiAdapter(850);
    }

    public IncomingPurchaseOrder(AmazonS3 testClient) {
        s3Client = testClient;
        edi850Adapter = new EdiAdapter(850);
    }

    @Override
    public String handleRequest(S3Event event, Context context) {
        InputStream edi850InputStream = getS3InputStream(event);

        // Archive Original 850
        Pair<PipedInputStream, TeeInputStream> inputStreamPair = duplicateInputStream(edi850InputStream);
        archiveEdiToGlacier(inputStreamPair.second);

        // Human Readable Csv Production
        inputStreamPair = duplicateInputStream(inputStreamPair.first);
        produceSaveNotifyHumanReadableCsv(inputStreamPair.second);

        // Data base Storage
        inputStreamPair = duplicateInputStream(inputStreamPair.first);
        saveRelevantDataToODS(inputStreamPair.second);

        // Produce 997 confirmation that Purchase Order was recieved
        inputStreamPair = duplicateInputStream(inputStreamPair.first);
        produceOrderConfirmation(inputStreamPair.second);
        
        // TODO: research approprate return values
        return "Ok";
    }

    private void archiveEdiToGlacier(InputStream inputStream) {
        String id = "PO ID?-" + (new Date());
        archiveToGlacier(vaultName, "PurchaseOrder-" + id + "-edi-850", inputStream);
    }

    private void archiveToGlacier(String vaultName, String description, InputStream inputStream) {
        AmazonGlacier client = AmazonGlacierClientBuilder.defaultClient();

        Pair<PipedInputStream, TeeInputStream> inputStreamPair = duplicateInputStream(inputStream);
        CheckedInputStream checkedInputStream = new CheckedInputStream(inputStreamPair.first, new CRC32());
        byte [] buffer = new byte[1024];
        int size = 0;
        try {
            while(checkedInputStream.read(buffer, 0, buffer.length) >= 0) {

            }
            size = inputStreamPair.second.available();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        long checksum = checkedInputStream.getChecksum().getValue(); //TODO verify this is appropriate method to calc checksum
        UploadArchiveRequest request = new UploadArchiveRequest()
                .withVaultName(vaultName)
                .withArchiveDescription(description)
                .withChecksum("" + checksum)
                .withBody(inputStreamPair.second)
                .withContentLength((long) size);
        UploadArchiveResult archiveResult = client.uploadArchive(request);
    }

    private void emailNotificationOf850Csv() {
        // TODO Implement.  Need to email that the csv is available in s3
        // [Accounts Recievable, TFB Sales Ops, Grand Central]
    }

    private Pair<PipedInputStream, TeeInputStream> duplicateInputStream(InputStream inputStream) {
        PipedInputStream duplicate1 = new PipedInputStream();
        TeeInputStream duplicate2 = null;
        try {
            duplicate2 = new TeeInputStream(inputStream, new PipedOutputStream(duplicate1));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return Pair.of(duplicate1, null);
        }
        return Pair.of(duplicate1, duplicate2);
    }

    private void produceOrderConfirmation(InputStream inputStream) {
        // TODO Implement Production of 997 and saving to s3
        EdiAdapter ediAdapter = new EdiAdapter(997);
        PipedInputStream ackInputStream = new PipedInputStream();
        try {
            PipedOutputStream csvOutputStream = new PipedOutputStream(ackInputStream);
            Order order = null; //TODO beanToEdi not generic currently needs to be so we don't use the order object here
            ediAdapter.beanToEdi(order, csvOutputStream);
        } catch (IOException | SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Pair<PipedInputStream, TeeInputStream> inputStreamPair = duplicateInputStream(ackInputStream);
        String id = "PO ID?-" + (new Date());
        archiveToGlacier(vaultName, "PurchaseOrder-" + id + "-edi-997", inputStreamPair.first);
        writeToS3Bucket(id + "-997.edi", edi997TargetBucket, inputStreamPair.second);
    }

    private void saveRelevantDataToODS(InputStream inputStream) {
        // TODO Implement database cataloging
        // PO #, Account #, Contract #, # of lines, Additional Contact information
    }

    private InputStream getS3InputStream(S3Event event) {
        S3EventNotificationRecord record = event.getRecords().get(0);
        String srcBucketName = record.getS3().getBucket().getName();
        String srcKey = record.getS3().getObject().getUrlDecodedKey();

        S3Object s3Object = s3Client.getObject(new GetObjectRequest(srcBucketName, srcKey));
        InputStream ediInputStream = s3Object.getObjectContent();
        return ediInputStream;
    }

    private void writeToS3Bucket(String filepath, String bucketName, InputStream inputStream) {
        ObjectMetadata metadata = new ObjectMetadata();
        long size = 0;
        try {
            size = inputStream.available();
        } catch(IOException e) {
            log.error(e.getMessage(), e);
        }
        metadata.setContentLength(size);
        s3Client.putObject(bucketName, filepath, inputStream, metadata);
    }

    public void produceSaveNotifyHumanReadableCsv(InputStream inputStream) {
        ByteArrayOutputStream csvOutputStream = new ByteArrayOutputStream();
        produceHumanReadableCsv(inputStream, csvOutputStream);
        InputStream csvInputStream = new ByteArrayInputStream(csvOutputStream.toByteArray());
        Pair<PipedInputStream, TeeInputStream> inputStreamPair = duplicateInputStream(csvInputStream);
        String id = "PO ID?-" + (new Date());
        archiveToGlacier(vaultName, "PurchaseOrder-" + id + "-csv", inputStreamPair.first);
        writeToS3Bucket(id + ".csv", csvTargetBucket, inputStreamPair.second);
        emailNotificationOf850Csv();
    }

    public void produceHumanReadableCsv(InputStream inputStream, OutputStream outputStream) {
        try {
            Order order =  edi850Adapter.ediToBean(inputStream);
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.newDocument();
            Element root = doc.createElement("list");
            doc.appendChild(root);

            String banValue = order.getInterchangeControlHeader().getInterchangeReceiverId();
            String purchaseOrderNumberValue = order.getBeginningSegmentForPurchaseOrder().getPurchaseOrderNumber();
            
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
            
            HashMap<String, Integer> idQualifiers = new HashMap<String, Integer>();
            idQualifiers.put("BP", 7);
            idQualifiers.put("IB", 8);
            idQualifiers.put("EN", 9);

            String [] headers = {"BAN","Purchase Order Number","PO Line Number","Quantity","Unit of Measurement","Unit Price","Description", "BP", "IB", "EN"};
            CSVPrinter printer = new CSVPrinter(outputStreamWriter, CSVFormat.DEFAULT.withHeader(headers));

            List<Po1> po1List = order.getPo1();
            for(Po1 po: po1List) {
                ArrayList<String> recordLine = new ArrayList<String>();

                // BAN
                recordLine.add(banValue);
                // Purchase Order Name
                recordLine.add(purchaseOrderNumberValue);
                // PO Line Number
                recordLine.add(po.getBaselineItemData().getAssignedIdentification());
                // Quantity
                recordLine.add(po.getBaselineItemData().getQuantityOrdered());
                // Unit of Measurement
                recordLine.add(po.getBaselineItemData().getUnitOrBasisForMeasurementCode());
                // Unit Price
                recordLine.add(po.getBaselineItemData().getUnitPrice());
                // Description
                String description = "";
                for(Pid pid : po.getPid()) {
                    if(!pid.getProductItemDescription().getItemDescriptionType().equals("F")) {
                        System.out.println("ERROR don't know how to handle non freform item descriptions. [" + pid.getProductItemDescription().getItemDescriptionType() + "]");
                    } else {
                        if(description != "") {
                            description += "\n";
                        }
                        description += pid.getProductItemDescription().getDescription();
                    }
                }
                recordLine.add(description);

                //qualifiers
                for(int i=0; i< idQualifiers.size(); i++) {
                    recordLine.add("N/A");
                }
                String idQualifier = po.getBaselineItemData().getProductServiceIdQualifier1();
                if(idQualifier != null) {
                    if(idQualifiers.containsKey(idQualifier)) {
                        recordLine.set(idQualifiers.get(idQualifier), po.getBaselineItemData().getProductServiceId1()); 
                    } else {
                        log.error("[" + idQualifier + "] is not a recognized Id Qualifier");
                    }
                }
                idQualifier = po.getBaselineItemData().getProductServiceIdQualifier2();
                if(idQualifier != null) {
                    if(idQualifiers.containsKey(idQualifier)) {
                        recordLine.set(idQualifiers.get(idQualifier), po.getBaselineItemData().getProductServiceId2()); 
                    } else {
                        log.error("[" + idQualifier + "] is not a recognized Id Qualifier");
                    }
                }
                idQualifier = po.getBaselineItemData().getProductServiceIdQualifier3();
                if(idQualifier != null) {
                    if(idQualifiers.containsKey(idQualifier)) {
                        recordLine.set(idQualifiers.get(idQualifier), po.getBaselineItemData().getProductServiceId3()); 
                    } else {
                        log.error("[" + idQualifier + "] is not a recognized Id Qualifier");
                    }
                }
                idQualifier = po.getBaselineItemData().getProductServiceIdQualifier4();
                if(idQualifier != null) {
                    if(idQualifiers.containsKey(idQualifier)) {
                        recordLine.set(idQualifiers.get(idQualifier), po.getBaselineItemData().getProductServiceId4()); 
                    } else {
                        log.error("[" + idQualifier + "] is not a recognized Id Qualifier");
                    }
                }
                idQualifier = po.getBaselineItemData().getProductServiceIdQualifier5();
                if(idQualifier != null) {
                    if(idQualifiers.containsKey(idQualifier)) {
                        recordLine.set(idQualifiers.get(idQualifier), po.getBaselineItemData().getProductServiceId5()); 
                    } else {
                        log.error("[" + idQualifier + "] is not a recognized Id Qualifier");
                    }
                }
                
                
                printer.printRecord(recordLine);
            }
            printer.flush();
            printer.close();
        } catch (IOException | SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.tmobile.lambda;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Date;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.glacier.AmazonGlacier;
import com.amazonaws.services.glacier.AmazonGlacierClientBuilder;
import com.amazonaws.services.glacier.model.UploadArchiveRequest;
import com.amazonaws.services.glacier.model.UploadArchiveResult;
import com.amazonaws.services.glacier.transfer.ArchiveTransferManager;
import com.amazonaws.services.glacier.transfer.ArchiveTransferManagerBuilder;
import com.amazonaws.services.glacier.transfer.UploadResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.models.s3.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.ibm.icu.impl.Pair;
import com.tmobile.edi.adapter.EdiAdapter;
import com.tmobile.edi.model.order.Order;

import org.apache.commons.io.input.TeeInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class OutgoingMontlyInvoice implements RequestHandler<S3Event, String> {

    private static final Logger log = LoggerFactory.getLogger(OutgoingMontlyInvoice.class);
    private static final String csvSourceBucket = "test-doe-1";
    private static final String edi810TargetBucket = "test-doe-2";
    private static final String vaultName = "glacierVault";

    private AmazonS3 s3Client;
    private EdiAdapter edi810Adapter;

    public OutgoingMontlyInvoice() {
        s3Client = AmazonS3ClientBuilder.defaultClient();
        edi810Adapter = new EdiAdapter(810);
    }

    public OutgoingMontlyInvoice(AmazonS3 testClient) {
        s3Client = testClient;
        edi810Adapter = new EdiAdapter(810);
    }

    @Override
    public String handleRequest(S3Event event, Context context) {
        InputStream csvInputStream = getS3InputStream(event);

        // Archive Original 810
        Pair<PipedInputStream, TeeInputStream> inputStreamPair = duplicateInputStream(csvInputStream);
        archiveCsvToGlacier(inputStreamPair.second);

        // Data base Storage
        inputStreamPair = duplicateInputStream(inputStreamPair.first);
        saveRelevantDataToODS(inputStreamPair.second);

        // Produce 810 Invoice
        inputStreamPair = duplicateInputStream(inputStreamPair.first);
        produceInvoice(inputStreamPair.second);
        
        // TODO: research approprate return values
        return "Ok";
    }

    private void archiveCsvToGlacier(InputStream inputStream) {
        String id = "Invoice ID?-" + (new Date());
        archiveToGlacier(vaultName, "Invoice-Monthly-" + id + "-csv", inputStream);
    }

    private void archiveToGlacier(String vaultName, String description, InputStream inputStream) {
        AmazonGlacier client = AmazonGlacierClientBuilder.defaultClient();

        Pair<PipedInputStream, TeeInputStream> inputStreamPair = duplicateInputStream(inputStream);
        CheckedInputStream checkedInputStream = new CheckedInputStream(inputStreamPair.first, new CRC32());
        byte [] buffer = new byte[1024];
        int size = 0;
        try {
            while(checkedInputStream.read(buffer, 0, buffer.length) >= 0) {

            }
            size = inputStreamPair.second.available();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        long checksum = checkedInputStream.getChecksum().getValue(); //TODO verify this is appropriate method to calc checksum
        UploadArchiveRequest request = new UploadArchiveRequest()
                .withVaultName(vaultName)
                .withArchiveDescription(description)
                .withChecksum("" + checksum)
                .withBody(inputStreamPair.second)
                .withContentLength((long) size);
        UploadArchiveResult archiveResult = client.uploadArchive(request);
    }

    private void emailNotificationOf810Edi() {
        // TODO Implement.  Need to email that the edi is available in s3
        // [Accounts Recievable, Monthly Invoice Team (Robert)]
    }

    private Pair<PipedInputStream, TeeInputStream> duplicateInputStream(InputStream inputStream) {
        PipedInputStream duplicate1 = new PipedInputStream();
        TeeInputStream duplicate2 = null;
        try {
            duplicate2 = new TeeInputStream(inputStream, new PipedOutputStream(duplicate1));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return Pair.of(duplicate1, null);
        }
        return Pair.of(duplicate1, duplicate2);
    }

    private void produceInvoice(InputStream inputStream) {
        // TODO Implement Production of 810
        PipedInputStream ackInputStream = new PipedInputStream();
        try {
            PipedOutputStream edi810OutputStream = new PipedOutputStream(ackInputStream);
            Order order = null; //TODO beanToEdi not generic currently needs to be so we don't use the order object here
            edi810Adapter.beanToEdi(order, edi810OutputStream);
        } catch (IOException | SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Pair<PipedInputStream, TeeInputStream> inputStreamPair = duplicateInputStream(ackInputStream);
        String id = "Invoice ID?-" + (new Date());
        archiveToGlacier(vaultName, "Invoice-Monthly-" + id + "-edi-810", inputStreamPair.first);
        writeToS3Bucket(id + "-monthly-810.edi", edi810TargetBucket, inputStreamPair.second);
        emailNotificationOf810Edi();
    }

    private void saveRelevantDataToODS(InputStream inputStream) {
        // TODO Implement database cataloging
    }

    private InputStream getS3InputStream(S3Event event) {
        S3EventNotificationRecord record = event.getRecords().get(0);
        String srcBucketName = record.getS3().getBucket().getName();
        String srcKey = record.getS3().getObject().getUrlDecodedKey();

        S3Object s3Object = s3Client.getObject(new GetObjectRequest(srcBucketName, srcKey));
        InputStream ediInputStream = s3Object.getObjectContent();
        return ediInputStream;
    }

    private void writeToS3Bucket(String filepath, String bucketName, InputStream inputStream) {
        ObjectMetadata metadata = new ObjectMetadata();
        long size = 0;
        try {
            size = inputStream.available();
        } catch(IOException e) {
            log.error(e.getMessage(), e);
        }
        metadata.setContentLength(size);
        s3Client.putObject(bucketName, filepath, inputStream, metadata);
    }
}

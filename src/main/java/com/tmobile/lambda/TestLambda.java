package com.tmobile.lambda;

import java.io.IOException;
import java.io.InputStream;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.models.s3.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestLambda implements RequestHandler<S3Event, String> {

    private static final Logger log = LoggerFactory.getLogger(TestLambda.class);
    private static final String targetBucket = "test-doe-2";

    private AmazonS3 s3Client;

    public TestLambda() {
        s3Client = AmazonS3ClientBuilder.defaultClient();
    }

    public TestLambda(AmazonS3 testClient) {
        s3Client = testClient;
    }

    @Override
    public String handleRequest(S3Event event, Context context) {

        S3EventNotificationRecord record = event.getRecords().get(0);
        String eventName = record.getEventName();

        // TODO: figure out if there is a good source of constants for this https://docs.aws.amazon.com/cdk/api/latest/java/software/amazon/awscdk/services/s3/EventType.html
        // TODO: evalute other object created methods and see if they need to be handled (ObjectCreated, ObjectCreatedCopy, ObjectCreatedPost)
        if(eventName != "ObjectCreated:Put") {
            String errorMessage = "[" + eventName + "] is an unsupported Event Type";
            log.warn(errorMessage);
            return errorMessage;
        }

        InputStream ediInputStream = getS3InputStream(event);

        String targetKey = "key";
        ObjectMetadata metadata = new ObjectMetadata();
        long ediSize = 0;
        try {
            ediSize = ediInputStream.available();
        } catch(IOException e) {
            log.error(e.getMessage(), e);
        }
        metadata.setContentLength(ediSize);
        s3Client.putObject(targetBucket, targetKey, ediInputStream, metadata);

        return "Ok";
    }

    private InputStream getS3InputStream(S3Event event) {
        S3EventNotificationRecord record = event.getRecords().get(0);
        String srcBucketName = record.getS3().getBucket().getName();
        String srcKey = record.getS3().getObject().getUrlDecodedKey();

        S3Object s3Object = s3Client.getObject(new GetObjectRequest(srcBucketName, srcKey));
        InputStream ediInputStream = s3Object.getObjectContent();
        return ediInputStream;
    }
}

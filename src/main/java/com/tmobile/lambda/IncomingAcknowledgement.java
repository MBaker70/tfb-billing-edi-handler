package com.tmobile.lambda;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Date;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.glacier.AmazonGlacier;
import com.amazonaws.services.glacier.AmazonGlacierClientBuilder;
import com.amazonaws.services.glacier.model.UploadArchiveRequest;
import com.amazonaws.services.glacier.model.UploadArchiveResult;
import com.amazonaws.services.glacier.transfer.ArchiveTransferManager;
import com.amazonaws.services.glacier.transfer.ArchiveTransferManagerBuilder;
import com.amazonaws.services.glacier.transfer.UploadResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.models.s3.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.ibm.icu.impl.Pair;
import com.tmobile.edi.adapter.EdiAdapter;
import com.tmobile.edi.model.order.Order;

import org.apache.commons.io.input.TeeInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class IncomingAcknowledgement implements RequestHandler<S3Event, String> {

    private static final Logger log = LoggerFactory.getLogger(IncomingAcknowledgement.class);
    private static final String edi997SourceBucket = "test-doe-1";
    private static final String vaultName = "glacierVault";

    private AmazonS3 s3Client;
    private EdiAdapter edi997Adapter;

    public IncomingAcknowledgement() {
        s3Client = AmazonS3ClientBuilder.defaultClient();
        edi997Adapter = new EdiAdapter(997);
    }

    public IncomingAcknowledgement(AmazonS3 testClient) {
        s3Client = testClient;
        edi997Adapter = new EdiAdapter(997);
    }

    @Override
    public String handleRequest(S3Event event, Context context) {
        InputStream edi997InputStream = getS3InputStream(event);

        // Archive Original 997
        Pair<PipedInputStream, TeeInputStream> inputStreamPair = duplicateInputStream(edi997InputStream);
        archiveEdiToGlacier(inputStreamPair.second);

        // Data base Storage
        saveRelevantDataToODS(inputStreamPair.first);

        // Notify AR Team
        emailNotificationOf997Edi();
        
        // TODO: research approprate return values
        return "Ok";
    }

    private void archiveEdiToGlacier(InputStream inputStream) {
        String id = "Invoice ID?-" + (new Date());
        String dailyMonthly = "Daily";
        archiveToGlacier(vaultName, "Invoice-" + dailyMonthly + "-" + id + "-edi-997", inputStream);
    }

    private void archiveToGlacier(String vaultName, String description, InputStream inputStream) {
        AmazonGlacier client = AmazonGlacierClientBuilder.defaultClient();

        Pair<PipedInputStream, TeeInputStream> inputStreamPair = duplicateInputStream(inputStream);
        CheckedInputStream checkedInputStream = new CheckedInputStream(inputStreamPair.first, new CRC32());
        byte [] buffer = new byte[1024];
        int size = 0;
        try {
            while(checkedInputStream.read(buffer, 0, buffer.length) >= 0) {

            }
            size = inputStreamPair.second.available();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        long checksum = checkedInputStream.getChecksum().getValue(); //TODO verify this is appropriate method to calc checksum
        UploadArchiveRequest request = new UploadArchiveRequest()
                .withVaultName(vaultName)
                .withArchiveDescription(description)
                .withChecksum("" + checksum)
                .withBody(inputStreamPair.second)
                .withContentLength((long) size);
        UploadArchiveResult archiveResult = client.uploadArchive(request);
    }

    private void emailNotificationOf997Edi() {
        // TODO Implement.  Need to email that the csv is available in s3
        // [Accounts Recievable]
    }

    private Pair<PipedInputStream, TeeInputStream> duplicateInputStream(InputStream inputStream) {
        PipedInputStream duplicate1 = new PipedInputStream();
        TeeInputStream duplicate2 = null;
        try {
            duplicate2 = new TeeInputStream(inputStream, new PipedOutputStream(duplicate1));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return Pair.of(duplicate1, null);
        }
        return Pair.of(duplicate1, duplicate2);
    }

    private void saveRelevantDataToODS(InputStream inputStream) {
        // TODO Implement database cataloging
    }

    private InputStream getS3InputStream(S3Event event) {
        S3EventNotificationRecord record = event.getRecords().get(0);
        String srcBucketName = record.getS3().getBucket().getName();
        String srcKey = record.getS3().getObject().getUrlDecodedKey();

        S3Object s3Object = s3Client.getObject(new GetObjectRequest(srcBucketName, srcKey));
        InputStream ediInputStream = s3Object.getObjectContent();
        return ediInputStream;
    }
}

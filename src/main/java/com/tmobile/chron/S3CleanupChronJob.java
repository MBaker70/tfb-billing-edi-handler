package com.tmobile.chron;

public class S3CleanupChronJob {
    public static final String monthlyCsvBucket = "monthlyCsv"; // keep 7 months initially
    public static final String dailyCsvBucket = "dailyCsv"; // keep 14 days initially
    public static final String edi997Bucket = "edi997"; // keep 14 days initially
    public static final String edi850Bucket = "edi850"; // keep 14 days initially
    public void cleanupS3() {
        // TODO orchestrate cleanup of files in above buckets if older than a configurable period of time
        // We are only cleaning up input buckets as output buckets should be monitored by whoever utilizes them,
        // we don't want to premtively delete something that they still need
    }
}

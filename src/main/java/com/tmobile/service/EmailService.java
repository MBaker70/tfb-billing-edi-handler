package com.tmobile.service;


import com.tmobile.constants.ServiceConstants;
import com.tmobile.model.RequestMessage;

public class EmailService {
    private KafkaService kafkaService;
    public EmailService(KafkaService kafkaService) {
        this.kafkaService=kafkaService;
    }

    public EmailService() {
        this.kafkaService=new KafkaService();
    }
    public void sendOrderReceipt(String orderNumber) {
        RequestMessage rm=new RequestMessage();
        rm.setTransType(ServiceConstants.TRANS_TYPE_EDI_ORDER_RECEIVED);
        rm.setEmail(ServiceConstants.ORDER_TO_ADDRESS);
        rm.setEdiOrderNumber(orderNumber);
        rm.setEdiOrderUIUrl(ServiceConstants.ORDER_UI_URL);
        rm.setTxnStatus(ServiceConstants.TXN_STATUS_SUCCESS);
        kafkaService.pushToQueue(rm);
    }

    public void sendOrderFailure(String orderNumber) {
        RequestMessage rm=new RequestMessage();
        rm.setTransType(ServiceConstants.TRANS_TYPE_EDI_ORDER_FAILURE);
        rm.setEmail(ServiceConstants.ORDER_TO_ADDRESS);
        rm.setEdiOrderNumber(orderNumber);
        rm.setTxnStatus(ServiceConstants.TXN_STATUS_FAILURE);
        kafkaService.pushToQueue(rm);
    }

    public void sendInvoiceSent(String invoiceNumber, String customerName) {
        RequestMessage rm=new RequestMessage();
        rm.setTransType(ServiceConstants.TRANS_TYPE_EDI_INVOICE_SENT);
        rm.setEmail(ServiceConstants.ORDER_TO_ADDRESS);
        rm.setEdiInvoiceNumber(invoiceNumber);
        rm.setEdiCustomerName(customerName);
        rm.setTxnStatus(ServiceConstants.TXN_STATUS_SUCCESS);
        kafkaService.pushToQueue(rm);
    }

    public void sendInvoiceAcknowledgementReceived(String invoiceNumber, String customerName) {
        RequestMessage rm=new RequestMessage();
        rm.setTransType(ServiceConstants.TRANS_TYPE_EDI_INVOICE_ACKNOWLEDGEMENT_RECEIVED);
        rm.setEmail(ServiceConstants.ORDER_TO_ADDRESS);
        rm.setEdiInvoiceNumber(invoiceNumber);
        rm.setEdiCustomerName(customerName);
        rm.setTxnStatus(ServiceConstants.TXN_STATUS_SUCCESS);
        kafkaService.pushToQueue(rm);
    }
}
package com.tmobile.service;

import com.tmobile.constants.ServiceConstants;
import com.tmobile.model.RequestMessage;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.Properties;

public class KafkaService {
    private KafkaProducer kafkaProducer;
    public KafkaService(KafkaProducer kafkaProducer){
        this.kafkaProducer=kafkaProducer;
    }

    public KafkaService(){
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, ServiceConstants.KAFKA_BROKER_LIST);
        props.put(JsonSerializer.ADD_TYPE_INFO_HEADERS, false);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        props.put(ProducerConfig.RETRIES_CONFIG, 0);
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
        props.put(ProducerConfig.LINGER_MS_CONFIG, 1);
        props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
        this.kafkaProducer=new KafkaProducer(props);;
    }

    public void pushToQueue(RequestMessage request){
        try{
            kafkaProducer.send(new ProducerRecord(ServiceConstants.KAFKA_EMAIL_TOPIC, request));
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            kafkaProducer.close();
        }
    }
}
